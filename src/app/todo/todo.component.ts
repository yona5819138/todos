import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TodosService } from '../todos.service';

@Component({
  selector: 'todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})

export class TodoComponent implements OnInit {
  @Input() data:any;
  @Output() myButtonClicked= new EventEmitter<any>();
  text;
  tempText;
  key;

showTheButton = false;
showEditField = false;

send(){
  console.log('event caught');
  this.myButtonClicked.emit(this.text);
}

showButton(){
this.showTheButton = true;
}
hideButton(){
  this.showTheButton = false;

}
deleteTodo(){
  this.todoService.deleteTodo(this.key);
}

showEdit(){
  this.showEditField = true;
  this.tempText = this.text;
  this.showTheButton =true;
}

save(){
this.todoService.update(this.key,this.text);
this.showEditField = false;
}

cancel(){
  this.showEditField=false;
  this.text= this.tempText;
}

  constructor(private todoService: TodosService) { }

  ngOnInit() {
    this.text = this.data.text;
    this.key = this.data.$key;

  }
  

}
